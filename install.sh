#!/usr/bin/env bash

set -e
set -u

# bash_traceback {{{
function bash_traceback() {
  local lasterr="$?"
  set +o xtrace
  local code="-1"
  local bash_command=${BASH_COMMAND}
  echo "Error in ${BASH_SOURCE[1]}:${BASH_LINENO[0]} ('$bash_command' exited with status $lasterr)" >&2
  if [ ${#FUNCNAME[@]} -gt 2 ]; then
    # Print out the stack trace described by $function_stack
    echo "Traceback of ${BASH_SOURCE[1]} (most recent call last):" >&2
    for ((i=0; i < ${#FUNCNAME[@]} - 1; i++)); do
    local funcname="${FUNCNAME[$i]}"
    [ "$i" -eq "0" ] && funcname=$bash_command
    echo -e "  ${BASH_SOURCE[$i+1]}:${BASH_LINENO[$i]}\\t$funcname" >&2
    done
  fi
  echo "Exiting with status ${code}" >&2
  exit "${code}"
}

# provide an error handler whenever a command exits nonzero
trap 'bash_traceback' ERR

# propagate ERR trap handler functions, expansions and subshells
set -o errtrace
# }}}

main() {
  local item
  local answer

  #
  # Checks
  #
  if [ "$(id -u)" -ne "0" ]; then
    echo "You the root privileges to run this script." >&2
    exit 1
  fi

  echo -n "Are you sure you want to run this script? [y,n] "; read -r answer
  if [[ "$answer" != "y" ]]; then
    echo "Interrupted." >&2
    exit 1
  fi

  #
  # Proxy
  #
  local proxy_server=''
  echo -n "Do you want to setup a proxy? [y,n] "; read -r answer
  if [[ "$answer" == "y" ]]; then
    ip addr
    echo
    echo -n "Enter the proxy (example: 192.168.1.1:3128): "; read -r proxy_server
  fi

  echo "Press enter to start..."; read -r answer

  #
  # SSH authorized keys
  #
  mkdir -p /root/.ssh
  cp .ssh/authorized_keys /root/.ssh/authorized_keys

  #
  # Packages
  #
  if which apt-get >/dev/null 2>&1; then
    if [[ $proxy_server != '' ]]; then
      echo "Acquire::http::proxy \"http://$proxy_server\";" > /etc/apt/apt.conf.d/99proxy
    fi

    # Confirm: Do you want to replace /etc/apt/sources.list
    echo -n "Do you want to replace /etc/apt/sources.list? [y,n] "; read -r ANSWER
    if [[ "$ANSWER" = "y" ]]; then
      if [[ $(lsb_release -s -i) = 'Debian' ]]; then
        {
          echo "deb http://mirror.csclub.uwaterloo.ca/debian $(lsb_release -s -c) main"
          echo "deb http://mirror.csclub.uwaterloo.ca/debian $(lsb_release -s -c)-updates main"
          echo "deb http://mirror.csclub.uwaterloo.ca/debian-security $(lsb_release -s -c)/updates main"
        } > /etc/apt/sources.list
        echo '[REPLACED] /etc/apt/sources.list'
      else
        echo "Error: the distribution $(lsb_release -s -i) is not supported." >&2
        exit 1
      fi
    fi

    apt-get update -q
    apt-get install -y -q python sudo openssh-server

    if [[ $(systemd-detect-virt) = 'kvm' ]]; then
      item=qemu-guest-agent
      echo "Install $item..."
      apt-get install -y -q "$item"
    fi
  elif which pacman >/dev/null 2>&1; then
    # pacman mirror
    # shellcheck disable=SC2016
    echo 'Server = http://mirror.csclub.uwaterloo.ca/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist

    # update
    local pacman_updated=0
    for item in python sudo openssh aria2; do
      pacman -Q "$item" >/dev/null 2>&1 && continue

      if [[ $pacman_updated -eq 0 ]]; then
        pacman_updated=1
        pacman -Syy
      fi

      pacman -S --noconfirm "$item" >/dev/null
    done

    # pacman config
    cp archlinux/pacman.conf archlinux/pacman-aria2.conf /etc/

    if [[ $proxy_server != '' ]]; then
      {
        echo "http-proxy=$proxy_server"
        echo "https-proxy=$proxy_server"
        echo "ftp-proxy=$proxy_server"
      } >> "/etc/pacman-aria2.conf"
    fi

    # kvm guest
    if [[ $(systemd-detect-virt) = 'kvm' ]]; then
      item=qemu-guest-agent
      echo "Install $item..."
      pacman -Q "$item" >/dev/null 2>&1 || pacman -S --noconfirm "$item" >/dev/null
    fi
  fi

  # Configure SSH
  local restart_openssh=0
  grep -v  -i 'PermitRootLogin' /etc/ssh/sshd_config > /etc/ssh/sshd_config.bak
  echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config.bak
  cmp /etc/ssh/sshd_config.bak /etc/ssh/sshd_config >/dev/null 2>&1 || restart_openssh=1
  mv -f /etc/ssh/sshd_config.bak /etc/ssh/sshd_config

  # Confirm: Unified kvm SSH keys
  echo -n "Unified kvm SSH keys? [y,n] "; read -r answer
  if [[ "$answer" = "y" ]]; then
    cp -a .sshd/* /etc/ssh/
    for item in /etc/ssh/*; do
      if [[ $item =~ _key$ ]]; then
        chmod 600 "$item"
      fi
    done
    restart_openssh=1
  else
    # reset the keys
    # Confirm: Do you want to reset openssh-server keys
    echo -n "Do you want to reset openssh-server keys? [y,n] "; read -r ANSWER
    if [[ "$ANSWER" == "y" ]]; then
      echo "Resetting the openssh key '/etc/ssh/ssh_host_rsa_key'..."
      rm /etc/ssh/ssh_host*key*
      ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
      restart_openssh=1
    fi
  fi

  if [[ $restart_openssh -eq 1 ]]; then
    echo "Restarting openssh-server..."
    systemctl restart sshd
  fi

  systemctl restart sshd
  echo "Success!"
  echo

  # Confirm: Delete ssh-init directory
  item=$(dirname "$0")
  item=$(readlink -e "$item")
  echo -n "Delete $item directory? [y,n] "; read -r ANSWER
  if [[ "$ANSWER" == "y" ]]; then
    echo "[DELETE] $item"
    rm -fr "$item"
  fi
}

main "$@"

# vim:ai:et:sw=2:ts=2:sts=2:tw=0:fenc=utf-8
